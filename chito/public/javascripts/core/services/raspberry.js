'use strict'
window.surround['app']
	.factory('raspberryService', ['$resource', '$cookies', function($resource, $cookies) {
	return $resource('/',
		{id : '@id'},
		{
			'led@toggle': {
				method: 'GET',
				url: '/raspberry/led/toggle/:status',
				params: {
					'status': '@status',
				},
			},
		});
	}]);