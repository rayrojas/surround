'strict';
window.surround['app']
	.controller("mainCtrl", ['$scope', '$timeout', 'raspberryService', '$firebaseObject',
		function($scope, $timeout, raspberryService, $firebaseObject) {
		var main = this;
		main.data = {
			'led': false,
			'fire': {
				'res': new Firebase("https://surrounds-429bc.firebaseio.com/led"),
				'node': {
					'led': undefined
				}
			},
			'status': {
				'led': {
					'get': 'sleep'
				}
			}
		};
		main.utils = {
			'turnOnLed': function() {
				if ( main.data.status.led.get === 'running' );
				main.data.status.led.get = 'running';
				raspberryService['led@toggle']({
					'status': main.data.led?'OFF':'ON'
				}, function(res){
					main.data.status.led.get = 'sleep';
					main.data.led = !main.data.led;
				}, function(err){
					main.data.status.led.get = 'crashed';
				});
			},
			'getText': function(){
				return main.data.led?'Apagar led':'Prender led';
			},
			'run': function() {
				main.data.fire.node.led = $firebaseObject(main.data.fire.res);
				main.data.fire.node.led.$loaded().then(function() {
					main.data.fire.node.led.$watch(function(callback, context) {
						main.utils.turnOnLed();
					});
				})
			}
		};
		$timeout(function(){
			main.utils.run();
		}, 1000);
	}]);